AJS.$(document).ready(function () {
    AJS.$("#connection_test").click(function () {
        // var confluenceUrl = AJS.$("#confluenceUrl").val();
        var userName = AJS.$("#userName").val();
        var userPassword = AJS.$("#userPassword").val();
        var configurationObject = {
            // confluenceUrl: confluenceUrl,
            userName: userName,
            userName: userPassword
        };
        $.ajax({
            url: contextPath + "/rest/project-creator/1.0/ProjectCreatorResource/testConnection",
            data: configurationObject,
            success: function () {
                AJS.messages.success("#configuration-header-id", {
                    title: 'Confluence connection',
                    body: '<p>Connection successful</p>'
                });
            },
            error: function () {
                AJS.messages.error("#configuration-header-id", {
                    title: 'Confluence connection',
                    body: '<p>Connection error! Check connection credentials</p>'
                });
            }
        });
    });
});
