package com.hypervelocity.project.creator;

import com.atlassian.jira.bc.projectroles.ProjectRoleService;
import com.atlassian.jira.project.Project;
import com.atlassian.jira.security.roles.ProjectRole;
import com.atlassian.jira.security.roles.ProjectRoleActors;
import com.atlassian.jira.security.roles.ProjectRoleManager;
import com.atlassian.jira.security.roles.RoleActor;
import com.atlassian.jira.util.ErrorCollection;
import com.atlassian.plugin.spring.scanner.annotation.imports.ComponentImport;
import com.google.common.collect.ImmutableList;

import java.util.Collection;
import java.util.Set;

public class ProjectRolesCopier {
    private final ProjectRoleManager projectRoleManager;
    private final ProjectRoleService projectRoleService;

    public ProjectRolesCopier(@ComponentImport ProjectRoleManager projectRoleManager,
                              @ComponentImport ProjectRoleService projectRoleService) {
        this.projectRoleManager = projectRoleManager;
        this.projectRoleService = projectRoleService;
    }

    public void copySettings(Project project, Project newProject, ErrorCollection errorCollection) {
        projectRoleManager.removeAllRoleActorsByProject(newProject);
        Collection<ProjectRole> roles = projectRoleManager.getProjectRoles(); // roles are globals, actors (users/groups) are project specific
        for(ProjectRole role : roles) {
            ProjectRoleActors projectActors = projectRoleManager.getProjectRoleActors(role, project);
            if (projectActors != null) {
                Set<RoleActor> realActors = projectActors.getRoleActors();
                if (realActors != null) {
                    for (RoleActor actor : realActors) {
                        projectRoleService.addActorsToProjectRole(ImmutableList.<String>of(actor.getParameter()), role, newProject,
                                actor.getType(), errorCollection);
                    }
                }
            }
        }
    }
}
