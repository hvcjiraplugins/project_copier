package com.hypervelocity.project.creator;

import com.atlassian.jira.project.Project;
import com.atlassian.jira.util.ErrorCollection;
import com.atlassian.jira.workflow.WorkflowSchemeManager;
import com.atlassian.plugin.spring.scanner.annotation.imports.ComponentImport;

public class WorkflowSchemeCopier extends AbstractSchemeCopier{


    public WorkflowSchemeCopier(@ComponentImport WorkflowSchemeManager workflowSchemeManager) {
        super(workflowSchemeManager);
    }


    public void copySettings(Project project, Project newProject, ErrorCollection errorCollection) {
        copyScheme(project, newProject);
    }
}
