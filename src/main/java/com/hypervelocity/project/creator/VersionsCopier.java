package com.hypervelocity.project.creator;


import com.atlassian.jira.exception.CreateException;
import com.atlassian.jira.project.Project;
import com.atlassian.jira.project.version.Version;

import com.atlassian.jira.project.version.VersionManager;
import com.atlassian.jira.util.ErrorCollection;
import com.atlassian.plugin.spring.scanner.annotation.imports.ComponentImport;

import java.util.List;

public class VersionsCopier {
    private final VersionManager versionManager;

    public VersionsCopier(@ComponentImport VersionManager versionManager) {
        this.versionManager = versionManager;
    }

    public void copySettings(Project project, Project newProject, ErrorCollection errorCollection) {
        List<Version> versions = versionManager.getVersions(project);
        for(Version version : versions) {
            try {
                versionManager.createVersion(version.getName(), version.getStartDate(), version.getReleaseDate(),
                        version.getDescription(), newProject.getId(), null, version.isReleased());
            } catch (CreateException e) {
                throw new RuntimeException(e);
            }

        }
    }
}
