package com.hypervelocity.project.creator;

import com.atlassian.jira.project.Project;
import com.atlassian.jira.scheme.Scheme;
import com.atlassian.jira.scheme.SchemeManager;
import com.atlassian.plugin.spring.scanner.annotation.imports.ComponentImport;

public abstract class AbstractSchemeCopier {

    private final SchemeManager schemeManager;

    public AbstractSchemeCopier(@ComponentImport SchemeManager schemeManager) {
        this.schemeManager = schemeManager;
    }

    public void copyScheme(Project project, Project newProject) {
        final Scheme scheme = schemeManager.getSchemeFor(project);
        if (scheme != null) {
            // it will actually remove all schemes of all types
            schemeManager.removeSchemesFromProject(newProject);
            schemeManager.addSchemeToProject(newProject, scheme);
        }
    }
}
