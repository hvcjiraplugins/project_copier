package com.hypervelocity.project.creator;


import com.atlassian.jira.project.Project;
import com.atlassian.jira.project.ProjectManager;
import com.atlassian.jira.util.ErrorCollection;
import com.atlassian.plugin.spring.scanner.annotation.imports.ComponentImport;

public class AssigneeTypeCopier {

    private final ProjectManager projectManager;

    public AssigneeTypeCopier(@ComponentImport ProjectManager projectManager) {
        this.projectManager = projectManager;
    }

    public void copySettings(Project project, Project newProject, ErrorCollection errorCollection) {
        Long assigneeType = project.getAssigneeType();

        projectManager.updateProject(newProject, newProject.getName(), newProject.getDescription(), newProject.getLeadUserName(),
                newProject.getUrl(), assigneeType);
    }
}
