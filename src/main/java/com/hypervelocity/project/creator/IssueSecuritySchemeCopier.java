package com.hypervelocity.project.creator;


import com.atlassian.jira.issue.security.IssueSecuritySchemeManager;
import com.atlassian.jira.project.Project;
import com.atlassian.jira.util.ErrorCollection;
import com.atlassian.plugin.spring.scanner.annotation.imports.ComponentImport;

public class IssueSecuritySchemeCopier extends AbstractSchemeCopier {


    public IssueSecuritySchemeCopier(@ComponentImport  IssueSecuritySchemeManager issueSecuritySchemeManager) {
        super(issueSecuritySchemeManager);
    }


    public void copySettings(Project project, Project newProject, ErrorCollection errorCollection) {
        copyScheme(project, newProject);
    }
}
