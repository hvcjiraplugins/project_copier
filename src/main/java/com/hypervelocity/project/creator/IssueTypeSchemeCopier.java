package com.hypervelocity.project.creator;


import com.atlassian.jira.component.ComponentAccessor;
import com.atlassian.jira.issue.IssueFieldConstants;
import com.atlassian.jira.issue.context.manager.JiraContextTreeManager;
import com.atlassian.jira.issue.customfields.CustomFieldUtils;
import com.atlassian.jira.issue.fields.FieldManager;
import com.atlassian.jira.issue.fields.config.FieldConfigScheme;
import com.atlassian.jira.issue.fields.config.manager.FieldConfigSchemeManager;
import com.atlassian.jira.issue.fields.config.manager.IssueTypeSchemeManager;
import com.atlassian.jira.project.Project;
import com.atlassian.jira.project.ProjectManager;
import com.atlassian.jira.util.ErrorCollection;
import com.atlassian.plugin.spring.scanner.annotation.imports.ComponentImport;
import com.google.common.collect.Iterables;

import java.util.List;

public class IssueTypeSchemeCopier {
    private final IssueTypeSchemeManager issueTypeSchemeManager;
    private final FieldConfigSchemeManager configSchemeManager;
    private final FieldManager fieldManager;
    private final ProjectManager projectManager;

    public IssueTypeSchemeCopier(@ComponentImport IssueTypeSchemeManager issueTypeSchemeManager,
                                 @ComponentImport FieldConfigSchemeManager configSchemeManager,
                                 @ComponentImport FieldManager fieldManager,
                                 @ComponentImport ProjectManager projectManager) {
        this.issueTypeSchemeManager = issueTypeSchemeManager;
        this.configSchemeManager = configSchemeManager;
        this.fieldManager = fieldManager;
        this.projectManager = projectManager;
    }

    public void copySettings(Project project, Project newProject, ErrorCollection errorCollection) {
        final FieldConfigScheme fieldConfigScheme = issueTypeSchemeManager.getConfigScheme(project);

        if (fieldConfigScheme != null) {
            final FieldConfigScheme newConfigScheme = issueTypeSchemeManager.getConfigScheme(newProject);
            if (fieldConfigScheme.equals(newConfigScheme)) {
                // the same scheme, no need to copy it
                return;
            }

            final List<Project> projects = fieldConfigScheme.getAssociatedProjectObjects();
            projects.add(newProject);

            final Long[] projectIds = Iterables.<Long>toArray(Iterables.transform(projects, CustomFieldsCopier.PROJECT_TO_ID), Long.class);

            // Set the contexts
            final List contexts = CustomFieldUtils.buildJiraIssueContexts(false, projectIds, projectManager);

            configSchemeManager.updateFieldConfigScheme(fieldConfigScheme, contexts,
                    fieldManager.getConfigurableField(IssueFieldConstants.ISSUE_TYPE));

            fieldManager.refresh();
        }
    }
}
