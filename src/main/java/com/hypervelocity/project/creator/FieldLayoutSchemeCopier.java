package com.hypervelocity.project.creator;


import com.atlassian.jira.issue.fields.layout.field.FieldConfigurationScheme;
import com.atlassian.jira.issue.fields.layout.field.FieldLayoutManager;
import com.atlassian.jira.project.Project;
import com.atlassian.jira.util.ErrorCollection;
import com.atlassian.plugin.spring.scanner.annotation.imports.ComponentImport;

public class FieldLayoutSchemeCopier {
    private final FieldLayoutManager fieldLayoutManager;

    public FieldLayoutSchemeCopier(@ComponentImport FieldLayoutManager fieldLayoutManager) {
        this.fieldLayoutManager = fieldLayoutManager;
    }

    public void copySettings(Project project, Project newProject, ErrorCollection errorCollection) {
        FieldConfigurationScheme scheme = fieldLayoutManager.getFieldConfigurationScheme(project);
        if (scheme != null) {
            fieldLayoutManager.addSchemeAssociation(newProject, scheme.getId());
        }
    }
}
