package com.hypervelocity.project.creator;

import com.atlassian.jira.permission.PermissionSchemeManager;
import com.atlassian.jira.project.Project;
import com.atlassian.jira.scheme.SchemeManager;
import com.atlassian.jira.util.ErrorCollection;
import com.atlassian.plugin.spring.scanner.annotation.imports.ComponentImport;

/**
 * Created by natalia on 29.11.16.
 */
public class PermissionSchemeCopier extends AbstractSchemeCopier {

    public PermissionSchemeCopier(@ComponentImport PermissionSchemeManager permissionSchemeManager) {
        super(permissionSchemeManager);
    }
    public void copySettings(Project project, Project newProject, ErrorCollection errorCollection) {
        copyScheme(project, newProject);
    }
}
