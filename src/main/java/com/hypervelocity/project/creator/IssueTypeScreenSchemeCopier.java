package com.hypervelocity.project.creator;

import com.atlassian.jira.issue.fields.FieldManager;
import com.atlassian.jira.issue.fields.config.manager.FieldConfigSchemeManager;
import com.atlassian.jira.issue.fields.screen.issuetype.IssueTypeScreenScheme;
import com.atlassian.jira.issue.fields.screen.issuetype.IssueTypeScreenSchemeManager;
import com.atlassian.jira.project.Project;
import com.atlassian.jira.util.ErrorCollection;
import com.atlassian.plugin.spring.scanner.annotation.imports.ComponentImport;

public class IssueTypeScreenSchemeCopier {
    private final IssueTypeScreenSchemeManager issueTypeScreenSchemeManager;
    private final FieldConfigSchemeManager configSchemeManager;
    private final FieldManager fieldManager;

    public IssueTypeScreenSchemeCopier(@ComponentImport IssueTypeScreenSchemeManager issueTypeScreenSchemeManager,
                                       @ComponentImport FieldConfigSchemeManager configSchemeManager,
                                       @ComponentImport FieldManager fieldManager) {
        this.issueTypeScreenSchemeManager = issueTypeScreenSchemeManager;
        this.configSchemeManager = configSchemeManager;
        this.fieldManager = fieldManager;
    }

    public void copySettings(Project project, Project newProject, ErrorCollection errorCollection) {
        final IssueTypeScreenScheme fieldConfigScheme = issueTypeScreenSchemeManager.getIssueTypeScreenScheme(project);
        if (fieldConfigScheme != null) {
            final IssueTypeScreenScheme newConfigScheme = issueTypeScreenSchemeManager.getIssueTypeScreenScheme(newProject);
            if (fieldConfigScheme.equals(newConfigScheme)) {
                return;
            }

            issueTypeScreenSchemeManager.removeSchemeAssociation(newProject, newConfigScheme);
            issueTypeScreenSchemeManager.addSchemeAssociation(newProject, fieldConfigScheme);
        }
    }
}
