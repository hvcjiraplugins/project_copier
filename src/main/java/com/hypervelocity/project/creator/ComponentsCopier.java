package com.hypervelocity.project.creator;

import com.atlassian.jira.bc.project.component.ProjectComponent;
import com.atlassian.jira.bc.project.component.ProjectComponentManager;
import com.atlassian.jira.project.Project;
import com.atlassian.jira.util.ErrorCollection;
import com.atlassian.plugin.spring.scanner.annotation.imports.ComponentImport;

import java.util.Collection;

public class ComponentsCopier {
    private final ProjectComponentManager projectComponentManager;

    public ComponentsCopier(@ComponentImport ProjectComponentManager projectComponentManager) {
        this.projectComponentManager = projectComponentManager;
    }
    public void copySettings(Project project, Project newProject, ErrorCollection errorCollection) {
        Collection<ProjectComponent> components = projectComponentManager.findAllForProject(project.getId());
        for(ProjectComponent component : components) {
            projectComponentManager.create(component.getName(), component.getDescription(), component.getLead(),
                    component.getAssigneeType(), newProject.getId());
        }
    }
}
