package com.hypervelocity.project.creator;


import com.atlassian.jira.notification.NotificationSchemeManager;
import com.atlassian.jira.project.Project;
import com.atlassian.jira.util.ErrorCollection;
import com.atlassian.plugin.spring.scanner.annotation.imports.ComponentImport;

public class NotificationsSchemeCopier extends AbstractSchemeCopier{

    public NotificationsSchemeCopier(@ComponentImport NotificationSchemeManager notificationSchemeManager) {
        super(notificationSchemeManager);
    }

    public void copySettings(Project project, Project newProject, ErrorCollection errorCollection) {
        copyScheme(project, newProject);
    }
}
