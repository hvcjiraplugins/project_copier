package com.hypervelocity.project.creator;


import com.atlassian.jira.config.ConstantsManager;
import com.atlassian.jira.issue.CustomFieldManager;
import com.atlassian.jira.issue.context.JiraContextNode;
import com.atlassian.jira.issue.customfields.CustomFieldUtils;
import com.atlassian.jira.issue.fields.CustomField;
import com.atlassian.jira.issue.fields.config.FieldConfig;
import com.atlassian.jira.issue.fields.config.FieldConfigScheme;
import com.atlassian.jira.issue.fields.config.manager.FieldConfigManager;
import com.atlassian.jira.issue.fields.config.manager.FieldConfigSchemeManager;
import com.atlassian.jira.issue.issuetype.IssueType;
import com.atlassian.jira.project.Project;
import com.atlassian.jira.project.ProjectManager;
import com.atlassian.jira.util.ErrorCollection;
import com.atlassian.plugin.spring.scanner.annotation.imports.ComponentImport;
import com.google.common.base.Function;
import com.google.common.collect.Iterables;
import com.google.common.collect.Lists;
import org.apache.commons.collections.MultiMap;
import org.ofbiz.core.entity.GenericValue;

import javax.annotation.Nullable;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class CustomFieldsCopier {

    private final CustomFieldManager customFieldManager;
    private final FieldConfigSchemeManager fieldConfigSchemeManager;
    private final ProjectManager projectManager;
    private final FieldConfigManager fieldConfigManager;

    public CustomFieldsCopier(@ComponentImport CustomFieldManager customFieldManager,
                              @ComponentImport FieldConfigSchemeManager fieldConfigSchemeManager,
                              @ComponentImport ProjectManager projectManager,
                              @ComponentImport FieldConfigManager fieldConfigManager) {
        this.customFieldManager = customFieldManager;
        this.fieldConfigSchemeManager = fieldConfigSchemeManager;
        this.projectManager = projectManager;
        this.fieldConfigManager = fieldConfigManager;
    }

    public static final Function<Project, Long> PROJECT_TO_ID = new Function<Project, Long>() {
        public Long apply(@Nullable Project input) {
            return input.getId();
        }
    };

    public void copySettings(Project project, Project newProject, ErrorCollection errorCollection) {

        final List<CustomField> customFields = customFieldManager
                .getCustomFieldObjects(project.getId(), ConstantsManager.ALL_ISSUE_TYPES);

        final Function<GenericValue, String> name = new Function<GenericValue, String>() {
            public String apply(@Nullable GenericValue input) {
                return input.getString("name");
            }
        };

        final Function<GenericValue, Long> gv2id = new Function<GenericValue, Long>() {
            public Long apply(@Nullable GenericValue input) {
                return input.getLong("id");
            }
        };

        for (CustomField customField : customFields) {
            if (!customField.isGlobal() && customField.isEnabled() && !customField.isAllProjects()) {
                final List<FieldConfigScheme> schemes = fieldConfigSchemeManager.getConfigSchemesForField(customField);
                for (FieldConfigScheme scheme : schemes) {
                    FieldConfigScheme configScheme = new FieldConfigScheme.Builder(scheme).toFieldConfigScheme();

                    // need to extend it if it's scope is limited to the project
                    final List<Project> projects = Lists.newArrayList(customField.getAssociatedProjectObjects());
                    if (!projects.contains(project)) {
                        continue; // this scheme doesn't include the original project skip it!
                    }

                    projects.add(newProject);

                    final Long[] projectIds = Iterables.<Long>toArray(Iterables.transform(projects, PROJECT_TO_ID), Long.class);

                    final List<JiraContextNode> contexts = CustomFieldUtils
                            .buildJiraIssueContexts(false, projectIds, projectManager);

                    final List<IssueType> issueTypes = customField.getAssociatedIssueTypes();

                    // Update so keep the old config
                    if (issueTypes != null) {
                        // Since we know that there is only one config
                        final Long configId = getFieldConfigIds(scheme)[0];
                        final FieldConfig config = fieldConfigManager.getFieldConfig(configId);
                        final Map<String, FieldConfig> configs = new HashMap<String, FieldConfig>(issueTypes.size());
                        for (final IssueType issueType : issueTypes) {
                            final String issueTypeId = issueType == null ? null : issueType.getId();
                            configs.put(issueTypeId, config);
                        }
                        configScheme = new FieldConfigScheme.Builder(configScheme).setConfigs(configs).toFieldConfigScheme();
                    }

                    // real update
                    fieldConfigSchemeManager.updateFieldConfigScheme(configScheme, contexts, customField);
                    customFieldManager.refreshConfigurationSchemes(customField.getIdAsLong());
                }
            }
        }
    }
    protected Long[] getFieldConfigIds(FieldConfigScheme configScheme) {
        // Set the config
        final MultiMap configMap = configScheme.getConfigsByConfig();
        if (configMap == null) {
            return new Long[0];
        } else {
            final Set entries = configScheme.getConfigsByConfig().keySet();
            final Long[] fieldConfigIds = new Long[entries.size()];
            int i = 0;
            for (final Object entry : entries) {
                final FieldConfig config = (FieldConfig) entry;
                fieldConfigIds[i] = config.getId();
                i++;
            }
            return fieldConfigIds;
        }
    }

}
