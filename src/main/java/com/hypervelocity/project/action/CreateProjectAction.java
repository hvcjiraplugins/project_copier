package com.hypervelocity.project.action;


import com.atlassian.jira.project.Project;
import com.atlassian.jira.project.ProjectManager;
import com.atlassian.jira.web.action.JiraWebActionSupport;
import com.atlassian.plugin.spring.scanner.annotation.component.Scanned;
import com.atlassian.plugin.spring.scanner.annotation.imports.ComponentImport;
import java.util.List;
@Scanned
public class CreateProjectAction extends JiraWebActionSupport {

    private final  ProjectManager projectManager;
    private String pkey;

    public CreateProjectAction(@ComponentImport ProjectManager projectManager) {
        this.projectManager = projectManager;
    }

    public String getPkey() {
        return pkey;
    }

    public void setPkey(String pkey) {
        this.pkey = pkey;
    }

    public List<Project> getAllProjects(){
        return projectManager.getProjects();
    }

    public void doCreateProjectByTemplate(){
        String projectKey = getPkey();

    }
}